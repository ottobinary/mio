Function Invio_email([string[]]$TaskName)
{
$smtpServer="172.17.5.2"
$from = "no-reply@contactlab.com"
$emailaddress = "backoffice_notification@contactlab.com"
$cc = "simone.nicosia@contactlab.com"
$subject = "(FMS16) Esito - $TaskName"
$body = "<b>$TaskName</b> eseguito correttamente in data $(Get-Date -Format 'dd/MM/yyyy HH:mm:ss')"
Send-Mailmessage -smtpServer $smtpServer -Port 25 -from $from -cc $cc -to $emailaddress -subject $subject -body $body -bodyasHTML
}
Invio_email $args